# Synthetix Uniswap tool

## Installation

Get Python requirements
```
pip3 install -r requirements.txt
```

Download export-0x4740c758859d4651061cc9cdefdba92bdc3a845d.csv from Etherscan: https://etherscan.io/exportData?type=address&a=0x4740c758859d4651061cc9cdefdba92bdc3a845d


## Running

See help:
```
./analyze.py -h
```
