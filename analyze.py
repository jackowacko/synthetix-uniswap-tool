#!/usr/bin/python3

from bs4 import BeautifulSoup as bs
from requests import get
from csv import DictReader
from os.path import isfile, isdir
from os import mkdir
from re import findall
from copy import deepcopy
import argparse

# Put disqualifications due to moving UNI-V1s and then withdrawing here
manual_disqualifications = []

def download(tx):
    path = 'cache/%s' % tx
    try:
        with open(path, 'r') as fp:
            return fp.read()
    except FileNotFoundError:
        pass
    print("Downloading %s" % tx)
    raw = get('https://etherscan.io/accountstatediff?m=normal&a=%s' % tx).text
    with open(path, 'w') as fp:
        fp.write(raw)
    return raw

def parse(raw):
    soup = bs(raw, features='lxml')

    a = soup.find(id='0x4740c758859d4651061cc9cdefdba92bdc3a845d')
    try:
        starteth, endeth = [float(findall(r'[0-9\.,]+', i.decode())[0].replace(',','')) for i in a.find_next_siblings('td')[2:4]]
    except AttributeError:
        return ('Failure', None, None, None, None)

    seth_blocks = soup(text='0x8361f6b072fc72af560d737ec174e72298ab36b34ff64672b91a6a862a52c723')[0].find_parent().find_parent().find_parent('div')
    startseth, endseth = [None, None]
    for p in seth_blocks:
        if 'before' in p.decode():
            startseth = int([findall(r'0000[0-9a-f]+', i)[0] for i in p.find_all('span')[-1]][0], 16)/10**18
        elif 'after' in p.decode():
            endseth = int([findall(r'000[0-9a-f]+', i)[0] for i in p.find_all('span')[-1]][0], 16)/10**18

    txtype = None
    if endeth > starteth and endseth > startseth:
        txtype = "Contribution"
    elif endeth < starteth and endseth > startseth:
        txtype = 'sETH -> ETH swap'
    elif endeth > starteth and endseth < startseth:
        txtype = 'ETH -> sETH swap'
    elif endeth == starteth and endseth == startseth:
        txtype = 'Failure'
    elif endeth < starteth and endseth < startseth:
        txtype = 'Withdrawl'
    else:
        raise RuntimeError('cannot categorize transaction')

    return (txtype, starteth, endeth, startseth, endseth)

def show_balances(balances):
    total = sum(balances.values())
    for k,v in sorted(balances.items()):
        if v > 0.0:
            print("%s: %.02f\t%.02f%%" % (k, v, v/total*100))

def show_row(row):
    if row['txtype'] != 'Failure':
        print("tx %s, block %s, sender %s, %s, ETH delta %f, sETH delta %f, start-ETH %s, start-sETH %s" % (row['Txhash'], row['Blockno'], row['From'], row['txtype'], row['endeth']-row['starteth'], row['endseth']-row['startseth'], row['starteth'], row['startseth']))
    else:
        print("tx %s, block %s, sender %s, %s" % (row['Txhash'], row['Blockno'], row['From'], row['txtype']))

def main():
    parser = argparse.ArgumentParser(description='Uniswap sETH/ETH pool: find arb rewards or calculate staking reward payouts')
    parser.add_argument('round', help="what round to find arb rewards from or two rounds between which to calculate staking rewards", nargs='+')
    parser.add_argument("-v", "--verbose", help="be verbose", action="store_true")
    args = parser.parse_args()

    verbose = args.verbose
    mode = None
    if len(args.round) == 2:
        mode = 'rewards'
    elif len(args.round) == 1:
        mode = 'arb'
    if mode == None:
        raise RuntimeError('Too many rounds given. Give one round for arb calculation or two rounds for staking rewards calculation. You gave %i rounds.' % len(args.round))
        
    data = []
    # Get this .csv from https://etherscan.io/exportData?type=address&a=0x4740c758859d4651061cc9cdefdba92bdc3a845d
    # It's behind a CAPCHA, so the script can't fetch it
    with open('export-0x4740c758859d4651061cc9cdefdba92bdc3a845d.csv') as fp:
        if not isdir('cache'):
            mkdir('cache')
        cr = DictReader(fp)
        for row in cr:
            raw = download(row['Txhash'])
            txtype, starteth, endeth, startseth, endseth = parse(raw)
            row.update({'txtype': txtype, 'starteth': starteth, 'endeth': endeth, 'startseth': startseth, 'endseth': endseth})
            data.append(row)
            if verbose:
                show_row(row)

    if mode == 'rewards':
        startblock, endblock = [int(i) for i in args.round]
        print("Using these manual disqualifications:")
        print(manual_disqualifications)
    else:
        startblock, endblock = (99999999999999, 99999999999999)
    start, end = (False, False)

    uni = 0.0
    balances = {}
    start_balances = None
    disqualified = manual_disqualifications
    for row in data:
        txtype = row['txtype']
        block = int(row['Blockno'])

        if block >= endblock and not end:
            end = True

        if txtype != 'Failure':
            sender = row['From']
            value = float(row['endeth']) - float(row['starteth'])
            if sender not in balances:
                balances[sender] = 0.0

            if txtype == 'Contribution':
                if uni <= 0.00000000001:
                    delta = value
                else:
                    delta = value * uni / row['starteth']
                uni += delta
                balances[sender] += delta

            elif txtype == 'Withdrawl':
                delta = value * uni / row['starteth']
                uni += delta
                balances[sender] += delta
                if balances[sender] < -0.00000000001:
                    print("Address %s has negative balance (%f). They're probably trying to cheat the rewards system -- go find all their sETH/ETH UNI-V1 addresses and blacklist them" % (sender, balances[sender]))
                if start and not end:
                    disqualified.append(sender)
                    if verbose:
                        print('%s withdrew between the given rounds (%s)' % (sender, row['Txhash']))

            elif txtype == 'ETH -> sETH swap' and mode == 'arb' and block > int(args.round[0]):
                ratio = row['starteth']/row['startseth']
                if ratio <= 0.99:
                    print('%s deserves arb reward for %s (ratio: %s)' % (sender, row['Txhash'], ratio))

        if block >= startblock and not start:
            start = True
            start_balances = deepcopy(balances)

    if mode == 'rewards':
        if not start :
            start_balances = deepcopy(balances)
        for i in disqualified:
            try:
                del start_balances[i]
            except KeyError:
                pass
        show_balances(start_balances)

if __name__ == '__main__':
    main()
